# Examples
The examples below EAS-Framework best-practices

[Simple Calculator](./example/calculator)
[Simple Gallery](./example/gallery)
[WS Chat](./example/chat)

## Git Repositories

[This Website](https://gitlab.com/eas-framework/eas-framework-website)